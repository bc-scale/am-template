# ansible-test

This project is used to manage Ansible test utilities in order to facilitate testting with Ansible provider

## Requirements

In order to develop in this role you may need:

* Docker: This is the provisioner that we will use to execute our tests.
* Pipenv: This is the python depencency management used to install tools required to work.


## Quickstart

To check tests examples:

```bash
# Ensure Docker is enabled beforee stating the process.

# Install repository dependecies locally for development
pipenv install --dev
pipenv shell
cd ansible/

# Install dependencies
pipenv run ansible-galaxy install -r requirements.yml

# Go inside the role
cd roles/tdd_example/

# Execute tests
molecule test
```
